# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
# All derivation framework formats must be listed here

# Example formats
from DerivationFrameworkExamples.TEST1 import TEST1Cfg
from DerivationFrameworkExamples.TEST2 import TEST2Cfg
from DerivationFrameworkExamples.TEST3 import TEST3Cfg
from DerivationFrameworkExamples.TEST4 import TEST4Cfg
from DerivationFrameworkExamples.TEST5 import TEST5Cfg
from DerivationFrameworkExamples.TEST6 import TEST6Cfg

# Avoids compilation warnings from Flake8
__all__ = ['TEST1Cfg','TEST2Cfg','TEST3Cfg','TEST4Cfg','TEST5Cfg','TEST6Cfg']
